provider "aws" {
  region = "ap-south-1" 
}

resource "aws_instance" "centos_instance" {
  ami           = "ami-0d0f77a5cdb8a51cd"  # Specify your desired AMI IDami-0a0f1259dd1c90938
  instance_type = "t3.micro"
  key_name      = "Mobile_Mum"     # Change this to your key pair name

  user_data = <<-EOF
              #!/bin/bash
              hostnamectl set-hostname c8.local
              EOF

  tags = {
    Name = "centos_instance"
  }
}

resource "aws_instance" "ubuntu_instance" {
    ami           = "ami-03f4878755434977f"  # Specify your desired AMI ID
  instance_type = "t3.micro"
  key_name      = "Mobile_Mum"     # Change this to your key pair name

  user_data = <<-EOF
              #!/bin/bash
              hostnamectl set-hostname u21.local
              EOF

  tags = {
    Name = "ubuntu_instance"
  }
}
output "frontend" {
  value = aws_instance.centos_instance.private_ip
}
output "backend" {
  value = aws_instance.ubuntu_instance.private_ip
}
output "ssh_key" {
  value = aws_instance.ubuntu_instance.key_name
}
output "backend_public_ip" {
  value = aws_instance.ubuntu_instance.public_ip
}

output "ansible_inventory" {
value = {
  frontend = aws_instance.centos_instance.public_ip
  backend = aws_instance.ubuntu_instance.public_ip
  ssh_key = aws_instance.ubuntu_instance.key_name
}
}
