#!/bin/bash

cat <<EOF > inventory.ini
[frontend]
centos_host ansible_ssh_host=$(terraform output -raw frontend) ansible_ssh_private_key_file=./$(terraform output -raw ssh_key).pem ansible_ssh_user=centos

[backend]
ubuntu_host ansible_ssh_host=$(terraform output -raw backend) ansible_ssh_private_key_file=./$(terraform output -raw ssh_key).pem ansible_ssh_user=ubuntu

[backend_public_ip]
$(terraform output -raw backend_public_ip)

EOF

